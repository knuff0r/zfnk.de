module.exports = {
  siteMetadata: {
    title: 'Zentrum für Naturheilkunde und Komplementärmedizin',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
	'gatsby-transformer-json',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-styled-components',
    'gatsby-plugin-sharp',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Zentrum für Naturheilkunde und Komplementärmedizin',
        short_name: 'zfnk',
        start_url: '/',
        background_color: '#568760',
        theme_color: '#568760',
        display: 'minimal-ui',
        icon: 'src/images/logo.png', // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-offline',
    'gatsby-plugin-react-leaflet',
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `Open Sans`,
          `Lato`,
          /*`source sans pro\:300,400,400i,700` // you can also specify font weights and styles*/
        ]
      }
    }
  ],
}
