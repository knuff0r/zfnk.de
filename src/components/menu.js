import React from 'react'
import {Link} from 'gatsby'
import styled from 'styled-components'


const LogoImage = styled.img`
  width: ${props => props.sticky ? '90px' : '0'};
  height: ${props => props.sticky ? '92px' : '0'};
  visibility: ${props => props.sticky ? 'visible' : 'hidden'};
  margin: 0;
  cursor: pointer;
  margin-right: 1em;
`;


class Menu extends React.Component {
  constructor(props) {
    super(props);


    this.handleHover = this.handleHover.bind(this);
    this.handleHoverEnd = this.handleHoverEnd.bind(this);

  }

  handleHover(state) {
    this.props.setHoverState(state);
  }

  handleHoverEnd() {
    this.props.setHoverState(null);
  }

  render() {
    return (
        <>

        </>
    );
  }
}

export default Menu
