import React from 'react'
import {Link} from 'gatsby'
import Menu from "./menu";
import LogoImage from "./images/logo";
import styled from "styled-components";

const StyledDiv = styled.div`
  background: #568760;
  padding: 5px;
  align-items: center;
  text-align:center;
  color: #DDD;
  font-size: medium;
`;

const Banner = ({siteTitle}) => (
    <StyledDiv>
      Lorem Ipsum
    </StyledDiv>
)

export default Banner
