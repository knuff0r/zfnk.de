import React from 'react'
import StickyComponent from 'react-stickynode'
export default class StickyFix extends React.Component {
  constructor(props) {
    super(props)
    this.mountSticky = (sticky) => { this.sticky = sticky }
    this.handleScroll = this.handleScroll.bind(this)
    this.state = {
      transformed: false,
    }
  }
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll)
    this.props.setInitialHeight(this.sticky.state.height);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }
  handleScroll() {
    setTimeout(() => {
      if (this.sticky) {
        this.sticky.updateInitialDimension();
        this.sticky.update()
        if(this.sticky.scrollTop > 64) {
          if(!this.state.transformed) {
            this.setState({transformed: true});
            this.props.transform();
          }
        }
        else {
          if(this.state.transformed) {
            this.setState({transformed: false});
            this.props.transform();
          }
        }
      }
    })
  }
  render() {
    return <StickyComponent ref={this.mountSticky} {...this.props} />
  }
}