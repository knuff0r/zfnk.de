const colors = {
  main_dark: '#222',
  main_light: '#fdffd4',
}

export default colors;