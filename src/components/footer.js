import React from 'react'
import {Link} from 'gatsby'
import Menu from "./menu";
import styled from "styled-components";
/*import { FaFacebook, FaYoutube, FaXing, FaEnvelope, FaPhone, FaMapMarker } from 'react-icons/fa';*/
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {fab} from '@fortawesome/free-brands-svg-icons'
import {faEnvelope, faPhone, faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons'

library.add(fab);
library.add(faEnvelope, faPhone, faMapMarkerAlt);


const MenuLink = styled(Link)`
  &:hover {
    color: #FFF;
  }
`

const MenuA = styled.a`
  &:hover {
    color: #FFF;
  }
`

const StyledLi = styled.li`
  color: #BBB;
  margin-right: 1rem;
  margin-bottom: 0.5rem;
  `
const StyledUl = styled.ul`
  list-style: none;
  `

const Social = styled(StyledUl)`
  display: flex;
`

const StyledFooter = styled.footer`
 padding: 1rem;
 display: flex;
 justify-content: flex-start;
 flex-wrap: wrap;
`
const Wrapper = styled.div`
display: flex;
justify-content: center;
`


const Footer = ({siteTitle}) => (
    <Wrapper>
      <StyledFooter>
        <StyledUl>
          <StyledLi><MenuLink to="/impressum">Impressum</MenuLink></StyledLi>
          <StyledLi><MenuLink to="/privacy">Datenschutz</MenuLink></StyledLi>
        </StyledUl>
        <StyledUl>
          <StyledLi><MenuA target="_blank" href="https://www.google.com/maps/place/Heilpraktiker+Czermak/@49.7149077,11.0934145,15z/data=!4m5!3m4!1s0x0:0x3107f3397ac02f19!8m2!3d49.7149077!4d11.0934145">
            <FontAwesomeIcon icon="map-marker-alt"/> Bayreuther Straße 106, 91301
            Forchheim</MenuA></StyledLi>
          <StyledLi><MenuLink to="/downloads"><FontAwesomeIcon icon="envelope"/> Zentrale@zfnk.de</MenuLink></StyledLi>
          <StyledLi><FontAwesomeIcon icon="phone"/> +49 9191 698-7878</StyledLi>
        </StyledUl>
        <Social>
          <StyledLi><MenuA target="_blank" href="https://www.facebook.com/ZFNK.de/?fref=ts"><FontAwesomeIcon icon={['fab', 'facebook']}/></MenuA></StyledLi>
          <StyledLi><MenuA target="_blank" href="https://www.youtube.com/channel/UCtJYJzQUwumEUdEyRIkQs-A"><FontAwesomeIcon icon={['fab', 'youtube']}/></MenuA></StyledLi>
          <StyledLi><MenuA target="_blank" href="https://www.xing.com/profile/Matthias_Czermak2"><FontAwesomeIcon icon={['fab', 'xing']}/></MenuA></StyledLi>
        </Social>
      </StyledFooter>
    </Wrapper>
)

export default Footer
