import Img from 'gatsby-image';
import React from 'react';
import Lightbox from "react-images";
import styled from "styled-components";

const Wrapper = styled.div`
`
const SuperWrapper = styled.div`
  width: 100%;
`
const SubWrapper = styled.div`
  padding: 1em;
  float: left;
`
const StyledImg = styled(Img)`
  width: 200px;
  height: 200px;
`
const Clear = styled.div`
  clear: both;
`

class Gallery extends React.Component {
  constructor(props) {
    super(props);
    console.log(props.photos);
    this.state = {
      shareOpen: false,
      anchorEl: null,
      lightbox: false,
      photos: props.photos.map(photo => Object.assign({
        src: photo.childImageSharp.fluid.src,
        srcSet: photo.childImageSharp.fluid.srcSet
      })),
    };
  }

  gotoPrevLightboxImage() {
    const {photo} = this.state;
    this.setState({photo: photo - 1});
  }

  gotoNextLightboxImage() {
    const {photo} = this.state;
    this.setState({photo: photo + 1});
  }

  openLightbox(photo, event) {
    event.preventDefault();
    this.setState({lightbox: true, photo});
  }

  closeLightbox() {
    this.setState({lightbox: false});
  }

  render() {
    const {photos} = this.props;
    return (
        <SuperWrapper>
          <Wrapper>
            {photos.map((photo, i) => (
                <SubWrapper>
                  <a key={i} href={photo.childImageSharp.fluid.src} onClick={e => this.openLightbox(i, e)}>
                    <StyledImg key={i} fluid={photo.childImageSharp.fluid} />
                  </a>
                </SubWrapper>
            ))}
          </Wrapper>
          <Lightbox
              backdropClosesModal
              images={this.state.photos}
              currentImage={this.state.photo}
              isOpen={this.state.lightbox}
              onClickPrev={() => this.gotoPrevLightboxImage()}
              onClickNext={() => this.gotoNextLightboxImage()}
              onClose={() => this.closeLightbox()}
          />
          <Clear />
        </SuperWrapper>
    );
  }
}

export default Gallery;