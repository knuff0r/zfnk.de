import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import {StaticQuery, graphql} from 'gatsby'
import styled from 'styled-components'

import Header from './header'
import './layout.css'
import Footer from "./footer";
import colors from "./colors";

const Wrapper = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
`

const MainContent = styled.div`
  flex: 1;
`

export const Content = styled.div`
  line-height: 1.55rem;
  margin: 0 auto;
  padding: 2em 1em;
  max-width: 1280px;
  color: #111;
`

const Body = styled.div`
  background: ${colors.main_light}
`


class Layout extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      sticky: false,
      headerHeight: 0,
      initialHeaderHeight: 0,
    }
    this.setSticky = this.setSticky.bind(this);
    this.setInitialHeaderHeight = this.setInitialHeaderHeight.bind(this);
    this.header = React.createRef();


  }

  setSticky(state, height) {
    this.setState({sticky: state, headerHeight: height});
  }

  setInitialHeaderHeight(height) {
    this.setState({initialHeaderHeight: height});
  }


  render() {
    let children = this.props.children;
    return (
        <StaticQuery
            query={graphql`
              query SiteTitleQuery {
              site {
              siteMetadata {
              title
              }
              }
              }
            `}
            render={data => (
                <Body>

                <Helmet
                      title={data.site.siteMetadata.title}
                      meta={[
                        {name: 'description', content: 'Gay'},
                        {name: 'keywords', content: 'sample, something'},
                      ]}
                  >
                    <html lang="en"/>
                  </Helmet>
                <Wrapper>
                  <div style={{background: colors.main_dark}}>
                    <Header ref={this.header} setInitialHeaderHeight={this.setInitialHeaderHeight}
                            setSticky={this.setSticky} siteTitle={data.site.siteMetadata.title}
                    />
                  </div>
                    <MainContent sticky={this.state.sticky}>
                      {children}
                    </MainContent>
                  <div style={{background: colors.main_dark}}>
                    <Footer/>
                  </div>
                </Wrapper>
                </Body>
            )}
        />
    );
  }
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
