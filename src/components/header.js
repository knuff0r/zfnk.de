import React from 'react'
import Sticky from 'react-stickynode';
import {Link} from 'gatsby'
import logo from "../images/logo.png";
import styled from "styled-components";
import Banner from "./banner";
import 'typeface-quattrocento';

import './header.css'
import StickyFix from "./StickyFix";

const StyledHeader = styled.header`
  background-color: ${props => props.sticky || !props.collapsed ? 'rgba(34, 34, 34, 0.9)' : 'none'};
  display: flex;
  flex-direction: ${props => props.collapsed ? 'row' : 'column'};
`;

const Wrapper = styled.div`
  margin: 0 auto;
  display: flex;
  justify-content: center;
  align-items: center;
`

const LogoImage = styled.img`
  width: ${props => props.show ? '90px' : '0'};
  /*height: ${props => props.show ? '92px' : '0'};*/
  height: 92px;
  opacity: ${props => props.show ? '1' : '0'};
  visibility: ${props => props.show ? 'visible' : 'hidden'};
  transition: ${props => props.show ? 'width 0.2s ease-in-out, visibility 0s ease 0.2s, opacity 0.1s linear 0.2s' : 'width 0.2s ease-in-out 0.1s, visibility 0s ease, opacity 0.1s linear'};
  cursor: pointer;
  margin-right: 1rem;
  position:relative;
  top: 15px;
  
  @media all and (max-width: 500px) {
  width: ${props => props.show ? '64px' : '0'};
  height: 64px;
    }
`;

const MenuLink = styled(Link)`
  color: #DDD;
  &:hover {
    color: #FFF;
  }
`

const StyledLi = styled.li`
  margin-right: 1rem;
`

const StyledUl = styled.ul`
  list-style: none;
  display: flex;
  flex-flow: row wrap;
  /* This aligns items to the end line on main-axis */
  @media all and (max-width: 800px) {
    justify-content: space-around;
  }
  @media all and (max-width: 500px) {
    flex-direction: column;
    display: ${props => props.collapsed ? 'none' : 'flex'};
    margin: ${props => props.position==="left" ? '1em 0 1em 1em' : '1em 1em 1em 0em'};
  }
  padding: 0;
  margin: ${props => props.position==="left" ? '0 0 0em 1em' : '0 1em 0em 0'};
  
`

const Toggle = styled.button`
  align-self:flex-start;
  margin-top: 0.3em;
  color: #DDD;
  font-weight: bold; 
  font-size: 1.2rem;
  border: none;
  background: none;
  outline: none;
  cursor: pointer;
  display: none;
  
  @media all and (max-width: 500px) {
    display: block;
  }
`


class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hover: null,
      sticky: false,
      initialHeight: 0,
      collapsed: true,
    };

    this.setHoverState = this.setHoverState.bind(this);
    this.transform = this.transform.bind(this);
    this.setInitialHeight = this.setInitialHeight.bind(this);
    this.toggle = this.toggle.bind(this);
    this.header = React.createRef();
  }

  setHoverState(state) {
    this.setState({hover: state});
  }

  transform() {
    this.setState({sticky: !this.state.sticky});
    this.props.setSticky(this.state.sticky, this.header.current.clientHeight);
  }

  toggle() {
    this.setState({collapsed: !this.state.collapsed});
  }

  setInitialHeight(height) {
    this.setState({initialHeight: height});
  }

  componentDidMount() {
    this.setInitialHeight(this.header.current.clientHeight);
    this.props.setInitialHeaderHeight(this.header.current.clientHeight);
  }

  render() {
    const handleStateChange = (status) => {
      const state = status.status === Sticky.STATUS_FIXED;
    }
    return (
        <>
          <Banner/>
          <StickyFix setInitialHeight={this.setInitialHeight} transform={this.transform} innerZ={1} enabled={true}
                     onStateChange={handleStateChange} >
            <StyledHeader collapsed= {this.state.collapsed} ref={this.header} sticky={this.state.sticky}>
              <Toggle onClick={this.toggle}>&#9776;</Toggle>
              <Wrapper>
              <StyledUl collapsed={this.state.collapsed} position={"left"}>
                <StyledLi><MenuLink
                    to="/healing">Heilkunde</MenuLink></StyledLi>
                <StyledLi><MenuLink
                    to="/coaching">Ordnungstherapie</MenuLink></StyledLi>
                <StyledLi><MenuLink
                    to="/wellness">Wellness</MenuLink></StyledLi>
                <StyledLi><MenuLink
                    to="/education">Akademie</MenuLink></StyledLi>
              </StyledUl>

              <LogoImage show={this.state.collapsed && this.state.sticky} onClick={() => window.scrollTo({top: 0, behavior: 'smooth'})}
                         src={logo} alt="Logo"/>
              <StyledUl collapsed={this.state.collapsed} position={"right"}>

                <StyledLi><MenuLink to="/about">Über uns</MenuLink></StyledLi>
                <StyledLi><MenuLink to="/consult">Sprechstunde</MenuLink></StyledLi>
                <StyledLi><MenuLink to="/contact">Kontakt</MenuLink></StyledLi>
                <StyledLi><MenuLink to="/downloads">Downloads</MenuLink></StyledLi>
              </StyledUl>
              </Wrapper>
            </StyledHeader>
          </StickyFix>
        </>
    );
  }
}


export default Header
