import React from 'react';
import {StaticQuery, graphql} from 'gatsby';
import Img from 'gatsby-image';

const WellnessImage = () => (
    <StaticQuery
        query={graphql`
      query {
        image: file(relativePath: { eq: "images/wellness_bw.png" }) {
          childImageSharp {
            fluid(maxWidth: 200) {
              ...GatsbyImageSharpFluid_noBase64
            }
          }
        }
      }
    `}
        render={(data) => <Img fluid={data.image.childImageSharp.fluid}/>}
    />
);

export default WellnessImage;

