import React from 'react';
import {StaticQuery, graphql} from 'gatsby';
import Img from 'gatsby-image';

const LogoHealImage = () => (
    <StaticQuery
        query={graphql`
      query {
        logoImage: file(relativePath: { eq: "images/logo_heal.png" }) {
          childImageSharp {
            fixed(width: 96) {
              ...GatsbyImageSharpFixed_noBase64
            }
          }
        }
      }
    `}
        render={(data) => <Img fixed={data.logoImage.childImageSharp.fixed}/>}
    />
);

export default LogoHealImage;

