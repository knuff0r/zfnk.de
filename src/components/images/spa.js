import React from 'react';
import {StaticQuery, graphql} from 'gatsby';
import Img from 'gatsby-image';

const SpaImage = () => (
    <StaticQuery
        query={graphql`
      query {
        image: file(relativePath: { eq: "images/spa.jpg" }) {
          childImageSharp {
            fixed(width: 400, quality: 80) {
              ...GatsbyImageSharpFixed_noBase64
            }
          }
        }
      }
    `}
        render={(data) => <Img fixed={data.image.childImageSharp.fixed}/>}
    />
);

export default SpaImage;

