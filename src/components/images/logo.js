import React from 'react';
import {StaticQuery, graphql} from 'gatsby';
import Img from 'gatsby-image';

const LogoImage = () => (
    <StaticQuery
        query={graphql`
      query {
        logoImageDefault: file(relativePath: { eq: "images/logo.png" }) {
          childImageSharp {
            fixed(width: 96) {
              ...GatsbyImageSharpFixed_noBase64
            }
          }
        }
      }
    `}
        render={(data) => <Img fixed={data.logoImageDefault.childImageSharp.fixed}/>}
    />
);

export default LogoImage;

