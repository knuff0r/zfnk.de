import React from 'react';
import {StaticQuery, graphql} from 'gatsby';
import Img from 'gatsby-image';

const TestImage = () => (
  <StaticQuery
    query={graphql`
      query {
        testImage: file(relativePath: { eq: "images/gatsby-astronaut.png" }) {
          childImageSharp {
            fluid(maxWidth: 300) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `}
    render={(data) => <Img fluid={data.testImage.childImageSharp.fluid} />}
  />
);

export default TestImage;
