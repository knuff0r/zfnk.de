import React, { Component } from 'react'
import {Map, Marker, Popup, TileLayer} from 'react-leaflet'

export default class ContactMap extends Component {
  render() {
    const options = this.props

    if (typeof window !== 'undefined') {
      return (
          <Map {...options}>
            <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Marker position={this.props.center}>
              <Popup>
                Heilpraktiker Matthias Czermak <br/>
                Bayreuther Straße 106
              </Popup>
            </Marker>
          </Map>
      )
    }
    return null
  }
}

