import React from 'react';
import {graphql, Link} from 'gatsby';

import Layout, {Content} from '../components/layout';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {library} from '@fortawesome/fontawesome-svg-core'
import {faFilePdf} from '@fortawesome/free-solid-svg-icons'
library.add(faFilePdf);

const DownloadPage = ({data}) => (
    <Layout>
      <div style={{background: '#fdffd4'}}>
        <Content>
          <DownloadSection name={"Allgemein/Praxis"} downloads={data.general.edges} />
          <DownloadSection name={"Rechtliches"} downloads={data.legal.edges} />
        </Content>
      </div>
    </Layout>
);

const DownloadSection = ({name, downloads}) => (
    <div>
    <h3>{name}</h3>
    <ul>
    {
      downloads.map((download,key) => (
          <li key={key}>
            <a href={download.node.file.relativePath} download>
              <FontAwesomeIcon icon="file-pdf"/>&nbsp;
              {download.node.name} <small>{download.node.file.prettySize}</small></a>
          </li>
      ))
    }
</ul>
    </div>

);

export const query = graphql`
{
  general:allGeneralJson {
    edges {
      node {
        name
        file {
          name
          prettySize
          relativePath
        }
      }
    }
  }
  legal:allLegalJson {
    edges {
      node {
        name
        file {
          name
          prettySize
          relativePath
        }
      }
    }
  }
}
`;

export default DownloadPage;
