import React from 'react';
import {Link} from 'gatsby';


import Layout, {Content} from '../components/layout';
import ContactMap from "../components/contactMap";
import styled from "styled-components";


const VehicleDirection = styled.div`
  margin-top: 1em;
`
const Vehicle = styled.div`
  text-decoration:underline;
`
const ContactForm = styled.form`
  max-width: 800px;
  margin: 0 auto;
  
  & > p > textarea {
    width: 100%; 
  }
`
const ContactWrapper = styled.div`
  @media all and (min-width: 800px) {
    display: flex;
  }
`
const LabelInput = styled.div`
  width: 100%;
  & > input {
    width: 100%;
  }
`

const StyledButton = styled.button `
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
`

const ContactPage = () => (
        <Layout>
          <div style={{background: '#fdffd4'}}>
            <Content>
              <h2>So finden Sie uns</h2>
              <ContactMap center={[49.714894, 11.093415]} zoom={13}/>
              <VehicleDirection>
                <Vehicle>Mit dem Auto:</Vehicle>
                <div>
                  <ul>
                    <li>
                      <strong>Von Bamberg kommend</strong>, nehmen Sie die Autobahnabfahrt „Forchheim Nord“ und folgen der
                      Bamberger Straße und später der Adenauerallee
                    </li>
                    <li>
                      An zentraler Kreuzung (am Einkaufsmarkt E-Center) nach links in Richtung Bahnhof / Bayreuth (B470)
                      halten.
                    </li>
                    <li>
                      Nach dem Sie die Stadtwerke Forchheim passiert haben (auffälliges Gebäude auf der linken Straßenseite)
                      links halten über die Brücke und der B470 in Richtung Bayreuth folgen.
                    </li>
                    <li>
                      Nach etwa 2km erreichen Sie das Ziel, welches rechts liegt.
                    </li>
                    <li>Die Heilpraxis befindet sich im Gebäude-Nr. 106 auf der rechten Seite (Orientierungspunkt ist ein
                      großer REWE-Parkplatz)
                    </li>
                  </ul>
                  <ul>
                    <li>
                      <strong>Aus Richtung Erlangen kommend</strong>, nehmen Sie die Autobahnabfahrt „Forchheim Süd“
                    </li>
                    <li>Folgen Sie der Hauptstraße Richtung Bahnhof / Bayreuth (B470)</li>
                    <li>Folgen Sie der B470 bis über die Brücke / Bahnhof und dann noch etwa 2km</li>
                    <li>Die Heilpraxis befindet sich im Gebäude-Nr. 106 auf der rechten Seite (Orientierungspunkt ist ein
                      großer REWE-Parkplatz)
                    </li>
                  </ul>
                </div>
              </VehicleDirection>
              <VehicleDirection>
                <Vehicle>Mit dem Bus</Vehicle>
                <ul>
                  <li>Vom Bahnhof fährt alle 15 Minuten ein Linienbus in Richtung Reuth / Ebermannstadt (Linien 222 / 223 /
                    262)
                  </li>
                  <li>Nach 5 Minuten steigen Sie an der Haltestelle „Forchheim Spinnerei“ aus (am REWE-Markt)</li>
                  <li>Die Praxis im Gebäude Nr. 106 erreichen Sie dann zu Fuß in nur 1 Minute</li>
                </ul>
              </VehicleDirection>


            </Content>
          </div>
          <div style={{background: '#ffd9bf'}}>
            <Content>
              <h2>Kontaktformular - schreiben Sie uns einfach</h2>
              <p>Lesen Sie Aktuelles aus unserer Praxis, Informationen zu Therapien oder Neuigkeiten aus Medizin, Ernährung
                und alternativer Gesundheitsförderung mit unserem Newsletter. Kostenlos, flexibel und unverbindlich.</p>
              <ContactForm name="contact" method="POST" data-netlify="true">
                <ContactWrapper>
                  <LabelInput style={{marginRight: "1em"}}><label>Name</label><br/> <input type="text"
                                                                                           name="name"/></LabelInput>
                  <LabelInput><label>Email</label><br/><input type="email" name="email"/></LabelInput>
                </ContactWrapper>
                <p>
                  <textarea name="message" rows="10" placeholder="Ihre Nachricht..."/>
                </p>
                <p>
                  <StyledButton type="submit">Send</StyledButton>
                </p>
              </ContactForm>
            </Content>
          </div>
        </Layout>
    )
;

export default ContactPage;
