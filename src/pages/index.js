import React from 'react';
import {Link} from 'gatsby';
import {graphql} from 'gatsby';
import trailer from '../videos/trailer.mp4';
import styled from "styled-components";
import logo from "../images/logo.png";

import Layout, {Content} from '../components/layout';

import 'typeface-satisfy';
import HealingImage from "../components/images/healing";
import EducationImage from "../components/images/education";
import CoachingImage from "../components/images/coaching";
import WellnessImage from "../components/images/wellness";

import thumbnail from "../images/gatsby-astronaut.png";

const Trailer = styled.video`
  width:100%;
  max-width:600px;
  height:auto;
  margin: 0 auto;
  display:inline-block;
  box-shadow: 0px 0px 20px 2px rgba(0,0,0,0.75);

`;

const ImageWrapper = styled.div`
  width: 15%;
  margin-right: 10%;
`;

const Title = styled.div`
  font-family: "Quattrocento", serif;
  font-weight: 500;
  color: #61A375;
  text-transform: uppercase;
  margin-top: 0.5rem;
`;

const LogoWrapper = styled.div`
  display:flex;
  justify-content: center;
`

const LogoImage = styled.img`
  width: 90px;
  height: 92px;
  margin: 0;
  cursor: pointer;
  margin-right: 1em;
`;

const Seperator = styled.hr`
  border-color: rgba(200, 200, 200, 0.1);
  margin-top: 1em;

`;


const IndexPage = () => {
  return (
      <Layout>
        <div style={{background: '#222'}}>
          <Content>
            <LogoWrapper>

              <LogoImage onClick={() => window.scrollTo({top: 0, behavior: 'smooth'})} src={logo} alt="Logo"/>
              <Title>
                <span style={{fontSize: '2.7rem'}}>Z</span><span style={{fontSize: '1.5rem'}}>entrum</span>
                <span style={{fontSize: '0.9rem'}}> für </span> <br/>
                <span style={{fontSize: '1.0em'}}> Naturheilkunde </span><span
                  style={{fontSize: '0.9rem'}}>und </span> <br/>
                <span style={{fontSize: '1.0rem'}}>Komplementärmedizin</span>
              </Title>
            </LogoWrapper>
            <Seperator/>
          </Content>
        </div>
        <div style={{background: '#fff2d1'}}>
          <Content>
            <p>
              Herzlich willkommen im Zentrum für Naturheilkunde und alternative Medizin.
            </p>
            <p>
              Auf den folgenden Seiten
              erhalten Sie Einblicke in unsere heilpraktische Arbeit und umfassende Informationen über unsere
              Philosophie
              und grundsätzlichen Überzeugungen.
            </p>
            <p>
              Um unserer Philosophie einer ganzheitlichen Sicht der Dinge Nachdruck zu verleihen, vereinigen wir
              bewährte
              traditionell abendländische Medizin (TAM) mit traditioneller chinesischer Medizin (TCM).
            </p>
            <p>
              Unser Bestreben liegt darin, Ihren Wunsch nach einer besseren Gesundheit, Wohlbefinden und Lebensqualität,
              auf eine für Sie passenden Weise zu unterstützen. Das Behandeln und Lindern von Beschwerden ist für uns
              genauso wichtig, wie die ganzheitliche Gesundheitsförderung und Prävention. Ein wesentlicher Baustein
              hierfür ist, alte Verhaltensmuster zu erkennen und konsequent zu verändern.
            </p>
            <p>
              Viele chronische Erkrankungen entstehen durch unsere täglichen Gewohnheiten. Wenn Sie für sich akzeptiert
              haben, dass umfassende Gesundheit ein Prozess ist, der Ihre Aufmerksamkeit und Ihre Energie benötigt, dann
              werden Sie es schaffen.
            </p>
            <p style={{
              fontFamily: 'satisfy',
              fontSize: '2rem',
            }}>
              Matthias Czermak
            </p>
            <p>
              Heilpraktiker
            </p>
            <div style={{display: 'flex'}}>
              <ImageWrapper>
                <HealingImage/>
              </ImageWrapper>
              <ImageWrapper>
                <EducationImage/>
              </ImageWrapper>
              <ImageWrapper>
                <CoachingImage/>
              </ImageWrapper>
              <ImageWrapper>
                <WellnessImage/>
              </ImageWrapper>
            </div>
          </Content>
        </div>
        <div style={{background: '#363636'}}>
          <Content style={{textAlign: 'center'}}>
            <Trailer
                src={trailer}
                controls
                poster={thumbnail}
            />
          </Content>
        </div>
        <div style={{background: '#DFCEBC'}}>
          <Content>
            <p>
              Wir schöpfen traditionelle Naturheilkunde und moderne Heilmethoden vollumfänglich aus, um Ihren
              Genesungsprozess weitere Schritte voran zu bringen. Gemeinsam mit Ihnen werden unsere Behandlungen ihre
              heilende Wirkung nachhaltig entfalten können. Dazu gehört auch, Körper und Geist wieder in eine
              harmonische
              Balance zu bringen. Gesundheit kann sich nur mit einem gesunden Geist entfalten.
            </p>
            <p>
              <u>Was sind unsere zentralen Therapiefelder?</u><br/>
              Der Schwerpunkt unserer Arbeit ist die Schmerztherapie. Von Rückenschmerzen über Muskel- und
              Nervenreizungen, Rheuma bzw. rheumatischen Erkrankungen bis hin zu Kopfschmerzen und Haltungskorrekturen
              gestaltet sich das Behandlungsfeld in unserer manuellen Therapie. Aber auch chronische Probleme in Magen
              und
              Darm, Unverträglichkeiten, Leistungsabfall bis hin zur heilkundlichen Förderung der Funktion von Lunge,
              Herz, Leber oder Niere gehören zu unserer täglichen Arbeit.
            </p>
            <p>
              <u>Was zeichnet das Heilkundezentrum-Forchheim aus?</u><br/>
              Wir sehen uns als wichtige Ergänzung zur Schulmedizin. Streben doch beide Berufsgruppen nach dem gleichen
              Ziel: Dem Patienten zu mehr Gesundheit zu verhelfen. In der Naturheilkunde jedoch, handelt jeder Patient
              hochgradig eigenverantwortlich.
            </p>
            <p>
              <u>Welche Behandlungen und Lösungsansätze suchen wir für Sie als Patienten?</u><br/>
              Vom Einfachen zum Komplexen und Schritt für Schritt. Wir sind bemüht alle notwendigen Maßnahmen so zu
              gestalten, dass diese in Ihren Alltag einfach zu integrieren sind. Die Erfahrung zeigt, nur dann halten
              Patienten auch mittelfristig ihre Hausaufgaben ein.
            </p>
            <p>
              <u>Wie beginnt eine heilkundliche Therapie?</u><br/>
              Das Fundament einer individuellen Behandlungsempfehlung bildet die Erstuntersuchung und Befunderhebung.
              Neben Ihren Informationen machen sich unsere Therapeuten ein eigenes Gesamtbild auf Basis aller
              verfügbaren
              Informationen und Testergebnisse. Hierauf baut sich dann ein erster Behandlungsvorschlag auf. Dieser
              beinhaltet neben den Therapien in der Praxis auch Ernährungstipps und Vorschläge zu Bewegung, Entspannung
              und Stressabbau.
            </p>
            <p>
              <u>Was kostet eine Therapiestunde?</u><br/>
              In Anlehnung an die Gebührenordnung für Heilpraktiker und die Beihilfeverordnung rechnen Sie bitte mit ca.
              70 Euro pro Behandlungsstunde. Abweichend davon, wenn Materialien und / oder Medikamente zum Einsatz
              kommen
              bzw. bei kürzerer oder längerer Behandlungsdauer. Gern kalkulieren wir vorab überschlägig die
              Behandlungshonorare als Orientierungshilfe.
            </p>
            <p>
              <u>Wie bekomme ich einen Termin?</u><br/>
              Wenn Sie persönliche Informationen oder einen Termin benötigen, dann nehmen Sie einfach telefonisch oder
              per
              Mail Kontakt zu uns auf. Die Kontaktdaten finden Sie im Impressum und am Ender dieser Seite.
            </p>
            <p>
              Herzlichst,<br/>
              Ihr Team vom ZFN-Forchheim.
            </p>
          </Content>
        </div>
      </Layout>
  );
}

function RenderLogo() {
}


export default IndexPage;
