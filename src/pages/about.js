import React from 'react';
import {graphql, Link} from 'gatsby';

import Layout, {Content} from '../components/layout';
import styled from "styled-components";
import Img from 'gatsby-image';
import Gallery from "../components/gallery";


const StyledContent = styled(Content)`
  @media all and (min-width: 800px) {
    display: flex;
    align-items: center;
  }
`
const StyledText = styled.div`
  @media all and (min-width: 800px) {
     margin-right: 2rem;
     flex: 1;
  }
`

const StyledImg = styled(Img)`
  @media all and (min-width: 800px) {
      flex: 1;
  }
`
const StyledFigCaption = styled.figcaption`
  font-size: 0.9rem;
`
const FigDiv = styled.div`
  @media all and (min-width: 800px) {
  display: flex;
   > * {
    flex: 1;
    }
  }
`
const IFWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`
const IFfigure = styled.figure`
`


const AboutPage = ({data}) => (
        <Layout>
          <div style={{background: '#fff3dd'}}>
            <StyledContent>
              <StyledText>
                <h2 id="phil">Philosophie</h2>
                <p>
                  Die Naturheilkunde hat sich weiterentwickelt. Das Bewährte wird mit neuen Ansätzen ständig weiter
                  verbessert. Moderne heilpraktische Therapien sollten vor allem die ganzheitliche Sicht auf die
                  Befindlichkeit des Menschen in den Vordergrund stellen.
                </p>

                <p>
                  Kein Therapieansatz ist gut genug, dass er als alleinige Anwendung langfristig wirksam ist. Nur eine
                  ganzheitliche Betrachtung hilft Ihnen Wege zu finden, auslösende Faktoren zu erkennen und zu verändern.
                  Hieraus entwickeln sich völlig neue Wege zur Unterstützung der geistigen und körperlichen
                  Gesundheit.
                </p>

                <p>
                  Hierfür steht das ZFN-Forchheim mit seinem Qualitätsversprechen.
                </p>
              </StyledText>
              <StyledImg fluid={data.spa.childImageSharp.fluid}/>
            </StyledContent>
            <div style={{background: '#DFCEBC'}}>
              <Content>
                <h2>Team</h2>
                <p>
                  In unserem Team verbinden wir unterschiedliche Persönlichkeiten und viele Facetten der Heilkunde zu einer
                  harmonischen Therapie. Jeder Therapeut hat hierbei seine ureigenen Stärken und Behandlungsfelder. Somit
                  schaffen wir es für unsere Kunden und Patienten, den passenden Behandlungsansatz zu finden. Darüber hinaus
                  ermöglichen wir dadurch Ihre Therapien zu verändern, zu ergänzen oder zu verfeinern.
                </p>
                <p>
                  Da die Heilkunde nie stillsteht, sondern sich weiterentwickelt, bilden wir uns im Team ständig in
                  verschiedenen Therapiegebieten weiter. So gewährleisten wir für Sie die bestmöglichste Vertrauensbasis.
                </p>
                <FigDiv>
                  <figure style={{maxWidth: '200px'}}>
                    <Img fixed={data.mat.childImageSharp.fixed}/>
                    <StyledFigCaption>
                      Matthias Czermak<br/>
                      Heilpraktiker
                    </StyledFigCaption>
                  </figure>
                  <p>
                    Matthias Czermak hat nach seinem sportwissenschaftlichen Fernstudium schnell den Weg in die
                    Sporttherapie eingeschlagen. Sein Behandlungsschwerpunkt ist die Schmerztherapie. Neben manuellen
                    osteopathischen Behandlungen sind die Neuraltherapie, Nosoden- und Eigenblutbehandlung, aber auch
                    begleitende Maßnahmen wie Taping, Ernährung und Phytotherapie wesentliche Elemente seiner Heilpraxis.
                    Seit über 10 Jahren ist er als Dozent und Ausbilder im Bereich der Naturheilkunde und
                    Gesundheitsförderung tätig.
                  </p>
                </FigDiv>
                <FigDiv>
                  <figure style={{maxWidth: '200px'}}>
                    <Img fixed={data.beate.childImageSharp.fixed}/>
                    <StyledFigCaption>
                      Beate Willner<br/>
                      Heilpraktikerin für traditionelle chinesische Medizin
                    </StyledFigCaption>
                  </figure>
                  <p>
                    Beate Willner fasziniert die alte chinesische Heilkunst seit vielen Jahren, weshalb sie nach ihrer
                    Ausbildung zur Heilpraktikerin die ‚Traditionelle Chinesische Medizin‘ (TCM) erlernte. Als
                    Krankenschwester sammelte sie umfangreiche Erfahrungen im Umgang mit Menschen und Patienten. Die
                    Fähigkeit, verschiedene medizinische Perspektiven mit traditionellen natürlichen Heilmethoden zu
                    vereinen, zählt zu ihren Stärken. Neben der Körperakupunktur zählen Ohrakupunktur, eine Ernährung nach
                    den 5 Elementen, sowie Kräuterheilkunde und die chinesische Massagetechnik „Gua Sha Fa“ (traditionelle
                    Massage mit einem Schaber) zu ihren Behandlungsschwerpunkten. Zahlreiche Aus- und Weiterbildungen sowie
                    ihr Bestreben Lösungen für Patienten zu finden, zeichnen sie in besonderem Maße als Therapeutin aus.
                  </p>
                </FigDiv>
              </Content>
            </div>
            <div style={{background: '#dfdac8'}}>
              <Content>
                <h2>Praxisrundgang</h2>
                <IFWrapper>
                  <IFfigure>
                    <div dangerouslySetInnerHTML={{__html: if0}}/>
                    <figcaption>Behandlungsraum 1</figcaption>
                  </IFfigure>
                  <IFfigure>
                    <div dangerouslySetInnerHTML={{__html: if1}}/>
                    <figcaption>Empfang / Zentrale</figcaption>
                  </IFfigure>
                  <IFfigure>
                    <div dangerouslySetInnerHTML={{__html: if2}}/>
                    <figcaption>Raum 2 – Labor</figcaption>
                  </IFfigure>
                </IFWrapper>
                <h4>Weitere Impressionen</h4>
                <Gallery photos={[
                    data.praxis0, data.praxis1, data.praxis2,
                    data.praxis3, data.praxis4, data.praxis5,
                    data.praxis6, data.praxis7, data.praxis8,
                ]}/>
              </Content>
            </div>
          </div>
        </Layout>
    )
;

const if0 = `<iframe src="https://cloud.panono.com/p/5IBlc00dsThZ/embed?autorotate=false&direction=right&speed=slow"
       width="460" height="280" frameborder="0" scrolling="no" allowfullscreen />`
const if1 = `<iframe src="https://cloud.panono.com/p/o18r3hqNfWnC/embed?autorotate=true&direction=right&speed=slow"
        width="460"  height="280" frameborder="0" scrolling="no" allowfullscreen></iframe>`
const if2 = `<iframe src="https://cloud.panono.com/p/kX0CBIsFZMUL/embed?autorotate=false&direction=right&speed=slow"
        width="460" height="280" frameborder="0" scrolling="no" allowfullscreen></iframe>`

export const query = graphql`
 query {
        spa: file(relativePath: { eq: "images/globuli.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 800, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        },
        mat: file(relativePath: { eq: "images/mat.jpg" }) {
          childImageSharp {
            fixed(width: 200, quality: 80) {
              ...GatsbyImageSharpFixed
            }
          }
        },
        beate: file(relativePath: { eq: "images/beate.png" }) {
          childImageSharp {
            fixed(width: 200, quality: 80) {
              ...GatsbyImageSharpFixed
            }
          }
        },
        praxis0: file(relativePath: { eq: "images/praxis/praxis0.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 1280, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        },
        praxis1: file(relativePath: { eq: "images/praxis/praxis1.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 1280, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        },
        praxis2: file(relativePath: { eq: "images/praxis/praxis2.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 1280, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        },
        praxis3: file(relativePath: { eq: "images/praxis/praxis3.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 1280, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        },
        praxis1: file(relativePath: { eq: "images/praxis/praxis1.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 1280, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        },
        praxis4: file(relativePath: { eq: "images/praxis/praxis4.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 1280, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        },
        praxis5: file(relativePath: { eq: "images/praxis/praxis5.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 1280, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        },
        praxis6: file(relativePath: { eq: "images/praxis/praxis6.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 1280, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        },
        praxis7: file(relativePath: { eq: "images/praxis/praxis7.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 1280, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        },
        praxis8: file(relativePath: { eq: "images/praxis/praxis8.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 1280, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        },
      }
      `

export default AboutPage;
