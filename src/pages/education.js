import React from 'react';
import {Link} from 'gatsby';

import Layout, {Content} from '../components/layout';

const AboutPage = () => (
    <Layout>
      <div style={{background: '#fdffd4'}}>
        <Content>
          <h2>Online-Sprechstunde / Telefon-Sprechstunde</h2>
          <p>
            Das Internet bietet heute Informationen im Überfluss. Fügt man purer Information nun Erfahrung hinzu,
            entsteht Wissen. Holen Sie sich spezifisches Wissen aus erster Hand und das im persönlichen Gespräch. Ganz
            einfach von zu Hause aus oder von unterwegs. Denn manchmal genügen wenige hilfreiche Tipps zu Unklarheiten,
            um Hindernisse zu überwinden. Ersparen Sie sich doch den Weg in die Praxis und die Zeit des Aufenthaltes
            dort, nur wegen Kleinigkeiten.
          </p>

          <p>
            ✓ Was bietet die Online-Sprechstunde?
            Die Online-Sprechstunde bietet Antworten zu grundsätzlichen medizinisch naturheilkundlichen Fragen wie
            naturheilkundliche Medikationen oder Behandlungen. Aber auch mögliche alternative Behandlungen, Zeiträume
            und vieles mehr. Im Kern umfasst die Online-Sprechstunde Leistungen des ZFN-Forchheim. Also Ernährung und
            andere Gewohnheiten sowie allgemeines und spezielles Wissen zu Heilung und Gesundheit. Durch die Möglichkeit
            der Voranmeldung (am unteren Ende dieser Seite), haben Sie immer den richtigen Ansprechpartner im Gespräch.
          </p>

          <p>
            ✓ Wann findet die Online-Sprechstunde statt?
            Wenn Sie sich vorher über unser Formular anmelden, können Sie uns Ihre Wunschzeit im Rahmen unserer
            Praxis-Öffnungszeiten vorschlagen. Darüber hinaus gibt es die „Offene Online-Sprechstunde“. Diese findet
            statt:
          </p>

          <p>
            ⇒ Immer Mittwoch – von 16.00 bis 18.00 Uhr <br/>
            ⇒ Immer Freitag – von 10.00 bis 12.00 Uhr
          </p>
          <p>
            Lesen Sie auch aufmerksam unsere weiteren Ausführungen und erhalten Sie zusätzliche wichtige Informationen
            zur Online-Sprechstunde und wie Sie diese richtig nutzen.
          </p>

        </Content>
      </div>
      <div style={{background: '#fff4e0'}}>
        <Content>
          <p>
            ✓ Wie können Sie Teilnehmer und Nutzer unserer Online-Sprechstunde werden?
            Das Webangebot ‚Skype‘ bietet kostenfrei Telefonie zwischen internetfähigen Medien an – und das als
            „End-to-End“ Verbindung. Beispielsweise vom Handy zum PC oder direkt von Rechner zu Rechner. Die Software
            funktioniert auf allen gängigen Computersystemen und Versionen, ist schnell installiert und unkompliziert
            anzuwenden. Fügen Sie nur unseren Skype-Namen „Heilpraktiker Czermak“ zu Ihren Kontakten hinzu und schon
            kann es losgehen.
          </p>

          <p>
            ✓ Wie nutze ich die Online-Sprechstunde richtig?
            Melden Sie sich im untenstehenden Formular als Gesprächsinteressent an und beschreiben Sie kurz worum es
            gehen soll. Im Rahmen der von Ihnen gewünschten Zeit können wir Sie oder Sie uns anwählen und ohne zeitliche
            Vorgabe das Thema erörtern und diskutieren.
            Sollte Ihr Anliegen spontan zu klären sein, dann können Sie unsere Therapeuten innerhalb der angebotenen
            offenen Online-Sprechstunde ohne Anmeldung anwählen. Im Rahmen unserer Möglichkeiten werden wir uns bemühen,
            etwas Licht ins Dunkel zu bringen.
          </p>

          <p>
            ✓ Was kann die Online-Sprechstunde nicht?
            Die Online-Sprechstunde erstellt keine Diagnosen. Sie ersetzt ebenso nicht den Besuch in der Praxis und die
            Untersuchung durch den Therapeuten, wenn derartiges notwendig ist. Auch die Online-Sprechstunde unterliegt
            der Schweigepflicht. Angaben zu Dritten werden innerhalb einer Beratung nicht gegeben.
            Unsere Online-Sprechstunde dient der Information und Aufklärung. Es werden von uns keine Trends gefördert
            oder negiert. Auch Meinungsbildung wird weder gefördert noch angeregt. Alles das ist unseriös und
            widerspricht den ethisch-moralische Grundsätzen, denen wir verpflichtet sind.
          </p>

          <p>
            ✓ Welche Kosten entstehen Ihnen als Nutzer?
            Die Nutzung von ‚Skype‘ als Internetanwendung (Verbindung zur Online-Sprechstunde via Computer oder W-LAN
            Handy) ist kostenfrei. Informieren Sie sich auf der Skype-Anwendung detailliert über alle Möglichkeiten. Je
            nach Telefontarif den Sie nutzen, können Telefonkosten entstehen. Zumeist sind Anrufe ins deutsche Festnetz
            heute kostenfrei. Bitte informieren Sie sich bei Ihrem Anbieter.
            Die Online-Sprechstunde als Dienstleistung des ZFN-Forchheim wird mit 5 Euro pro angefangene 5 Minuten in
            Rechnung gestellt. Mit der Eröffnung des Gespräches bzw. mit Ihrer Anmeldung über unser Anmeldeformular,
            erkennen Sie die AGB und die Kostenpflicht lt. BGB §611 an.
          </p>
        </Content>
      </div>
    </Layout>
);

export default AboutPage;
