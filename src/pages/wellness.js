import React from 'react';
import {Link} from 'gatsby';

import Layout from '../components/layout';

const WellnessPage = () => (
    <Layout>
      <h1>Hi from the second page</h1>
      <p>ABoutt to page 2</p>
      <Link to="/">Go back to the homepage</Link>
    </Layout>
);

export default WellnessPage;
