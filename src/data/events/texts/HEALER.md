Als unmittelbare Vorbereitung auf die Heilpraktikerprüfung, werden die Krankheits-Symptome differentialdiagnostisch verknüpft und mit wichtigen Laborwerten verbunden. Das Seminar wird Ihnen helfen, die vielen Puzzleteile zusammenzufügen.

„Selbständig im Heilberuf arbeiten“ - Eine Überprüfung zum Heilpraktiker ist sehr anspruchsvoll. Das geprüfte Wissen umfasst schulmedizinische Kenntnisse über zahlreiche Erkrankungen, ihr Entstehen, wie sie sich symptomatisch zeigen und welcher Nachweis mögliche ist. Hinzu kommen Gesetze sowie praktische Fähigkeitsnachweise in Injektion, Punktion und körperlicher Untersuchung.

Als unmittelbare Vorbereitung auf die Heilpraktikerprüfung, werden die Krankheits-Symptome differentialdiagnostisch verknüpft und mit wichtigen Laborwerten verbunden. Das Seminar wird Ihnen helfen, die vielen Puzzleteile zusammenzufügen. Inhalte:

- Diagnostisches Vorgehen in Theorie und Praxis
- Leitsymptome der Erkrankungen mit wesentlichen Laborwerten
- Körperliche Untersuchungen und Tests
- Notfälle und ‚Erste Hilfe‘
- Infektionsschutzgesetz, Hygiene und Rechtliches
- Tipps und Tricks für die mündliche Prüfung

Nachdem Sie die Phase der Abendkurse und intensivem eigenverantwortlichem Lernen absolviert haben, schließen wir die Prüfungsvorbereitung mit zwei kompakten und intensiven „Wochenend-Paukkursen“ ab. Danach haben Sie noch etwas Zeit, um letzte Wissenslücken bis zur schriftlichen Prüfung auszugleichen.