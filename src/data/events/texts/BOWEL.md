Themenabend über aktuelle wissenschaftliche Erkenntnisse über die Auswirkungen der Darmgesundheit auf systemische Erkrankungen.

Endlich haben Wissenschaftler bewiesen, was die TCM seit 4000 Jahren erfolgreich anwendet: „Darm und Immunsystem stehen in enger Verbindung miteinander“. 
Untersuchungen zeigen, dass autoimmune Erkrankungen wie Rheuma, Psoriasis, Hashimoto, Diabetes Typ1 oder gar Multiple Sklerose möglicherweise eine gemeinsame Ursache haben, einen „undichten Darm“.

Da Körper, Geist und Seele eine Einheit bilden, wird auch unsere Psyche und ihre Interaktion mit dem Darm zunehmend genauer betrachtet.
Etwa Erkrankungen wie Depressionen, Angstzustände, Schlafstörungen oder Essstörungen, ihren Zusammenhang mit der Darmgesundheit und welche natürlichen Therapie- und Heilungschancen es für den Alltag gibt.

Darüber hinaus beschäftigen wir uns mit der Frage, ob Ernährung, Fasten und Diäten, in Zukunft ganz neu zu bewerten sind.
Welche Rolle eine funktionelle Darmflora und eine strukturelle Gewebesubstanz in der Prävention spielen und wie Sie mit einfachen Mitteln beide Fundamente stärken können. Diese und weitere überraschende Erkenntnisse könnten Sie bereichern.