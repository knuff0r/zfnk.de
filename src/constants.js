export const HOVER_STATE = {
  HEAL: "heal",
  ACADEMY: "academy",
  THERAPY: "therapy",
  WELLNESS: "wellness",
}